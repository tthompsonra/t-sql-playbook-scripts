USE Chinook;
GO

-- SELECT all columns and rows from Artist table
SELECT * FROM Artist;

-- SELECT all columns and rows from Album table
SELECT * FROM Album;