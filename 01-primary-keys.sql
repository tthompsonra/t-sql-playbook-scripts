USE BigMachine;
GO


-- CREATE TABLE that auto increments the primary key 
-- INTEGER does have a LIMIT of 2,147,483,647
CREATE TABLE Users(
	Id INTEGER PRIMARY KEY IDENTITY(1, 1),
	email VARCHAR(50)
);

/*
Unique but takes up more space. Can use newsequentialid to help
with indexes
CREATE TABLE Users(
	Id UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID(),
	email VARCHAR(50)
);
*/