USE Chinook;
GO

SELECT * FROM Artist WHERE Name LIKE '%Kiss%';

UPDATE Artist SET Name = LTRIM(Name);

SELECT * FROM Artist WHERE Name = 'Kiss';