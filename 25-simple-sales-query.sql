USE Chinook;
GO

SELECT FirstName + ' ' + LastName AS Customer, BillingCountry, 
SUM(UnitPrice * Quantity) AS SalesTotal
--AVG(UnitPrice * Quantity) AS AvgPurchase
FROM Invoice
INNER JOIN Customer
ON Invoice.CustomerId = Customer.CustomerId
INNER JOIN InvoiceLine
ON InvoiceLine.InvoiceId = Invoice.InvoiceId
GROUP BY FirstName, LastName, BillingCountry
ORDER BY BillingCountry, LastName;