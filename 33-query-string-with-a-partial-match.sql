USE Chinook;

GO

-- setup a seek versus a full table scan on name index
CREATE INDEX IDX_ARTIST_NAME ON Artist(Name);

-- will seek records versus scan the whole table now
SELECT * FROM Artist
WHERE Name LIKE 'AC/DC%';

-- will have to scan the whole table no matter what
-- known as a non-SARGable query
SELECT * FROM Artist
WHERE Name LIKE '%AC/DC%';