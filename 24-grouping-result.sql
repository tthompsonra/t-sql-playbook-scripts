USE Chinook;
GO

SELECT BillingCountry,
SUM(TOTAL) AS AllTimesSales
FROM Invoice
-- GROUP BY makes it where the aggreagate 
-- Can work with a SELECT 
GROUP BY BillingCountry
ORDER BY AllTimesSales DESC