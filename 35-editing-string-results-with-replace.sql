USE Chinook;
GO

SELECT * FROM Album

SELECT Name, Title, REPLACE(Title, 'Blood', 'B***d') AS [Clean Title]
FROM Album 
INNER JOIN Artist
ON Album.ArtistId = Artist.ArtistId
WHERE Title LIKE '%Blood%';