USE Chinook;
GO
-- Sales report for Germany, Argentina, and UK
-- Only show sales above 40 dollars
SELECT BillingCountry, 
SUM(UnitPrice * Quantity) AS SalesTotal
FROM Invoice
INNER JOIN InvoiceLine
ON InvoiceLine.InvoiceId = Invoice.InvoiceId
WHERE BillingCountry IN('Germany', 'Argentina', 'United Kingdom')
GROUP BY BillingCountry
HAVING SUM(UnitPrice * Quantity) > 40
ORDER BY BillingCountry;