USE Chinook;

GO

-- INNER JOIN 
-- Only return data that is match from 
-- BOTH tables
SELECT Name, Title
FROM Artist 
INNER JOIN Album
ON Album.ArtistId = Artist.ArtistId;

-- LEFT JOIN
-- Returns all data from the left table
-- which in this case is Artist
-- And will return non-matched, NULL data from
-- the right table which in this case is Album
SELECT Name, Title
FROM Artist 
LEFT JOIN Album
ON Album.ArtistId = Artist.ArtistId;

-- RIGHT JOIN
-- Returns all data from the right table
-- in this case Album
-- that matches the ON statement
-- and will return null data 
-- from the left table if not found
-- in this case Artist
SELECT Name, Title
FROM Artist 
RIGHT JOIN Album
ON Album.ArtistId = Artist.ArtistId