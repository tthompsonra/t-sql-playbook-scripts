USE Chinook;
GO

SELECT Name, LEN(Name) 
FROM Artist
-- DATALENGTH will return length of data in column w/o trimming it like LEN would do
-- <> means not equal in SQL
WHERE DATALENGTH(Name) <> DATALENGTH(RTRIM(Name));

-- Set the RTRIM on all the data to
-- be correct
UPDATE ARTIST 
SET Name = RTRIM(Name);