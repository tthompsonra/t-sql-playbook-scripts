USE Chinook;

GO

-- FULL JOINS
-- Gets all the data from both the left and right table
-- Nulls will show up for what is not a match

SELECT Name, Title
FROM Artist 
FULL JOIN Album
ON Album.ArtistId = Artist.ArtistId;