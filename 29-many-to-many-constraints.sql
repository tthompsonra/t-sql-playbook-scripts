USE Chinook;

GO

DROP TABLE Users;
DROP TABLE Role;
DROP TABLE UserRole;

CREATE TABLE Users(
	Id INTEGER PRIMARY KEY NOT NULL IDENTITY(1, 1),
	Email VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE Role(
	Id INTEGER PRIMARY KEY NOT NULL IDENTITY(1, 1),
	Name VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE UserRole(
	-- REFERENCES sets a Foreign Key Constraint 
	-- To the other table
	-- ON DELETE CASCADE deletes any records 
	-- from the FK table if the item is deleted
	UserId INTEGER REFERENCES Users(Id) ON DELETE CASCADE,
	RoleId INTEGER REFERENCES Role(Id) ON DELETE CASCADE,
	PRIMARY KEY(UserId, RoleId)
);

INSERT INTO Users(email)
VALUES ('test@test.com');

INSERT INTO Role(name)
VALUES ('Adminstrator');

INSERT INTO UserRole(UserId, RoleId)
VALUES (1, 1);

SELECT Name, Email
FROM UserRole
INNER JOIN Users 
ON Users.Id = UserRole.UserId
INNER JOIN Role 
ON Role.Id = UserRole.RoleId