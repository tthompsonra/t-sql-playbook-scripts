USE Chinook;
GO

SELECT FirstName, LastName,
(SELECT FirstName + ' ' + LastName 
	FROM Employee bosses 
	WHERE Employee.ReportsTo = bosses.EmployeeId) AS boss
FROM Employee;