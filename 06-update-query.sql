-- Using Primary Key Constraint
UPDATE Users SET 
CreatedAt = '09/23/2018'
WHERE Id = 3;

-- UPDATE using a different constraint
UPDATE Users SET
CreatedAt = '09/23/2017'
WHERE Email = 'luisg@embraer.com.br';

SELECT * FROM Users;