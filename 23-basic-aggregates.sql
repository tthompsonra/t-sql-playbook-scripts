USE Chinook;
GO

SELECT 
-- Get the SUM of everything in the column
SUM(Total) AS AllTimeSales, 
-- Get the AVG of everything in the column
AVG(Total) AS AvgSale,
-- Get the COUNT of everything in the column
COUNT(Total) AS SalesCount,
-- Get MIN (smallest item) in the column
MIN(Total) AS SmallestSale,
-- Get the MAX (largest item) in the column
MAX(Total) AS BiggestSale
FROM Invoice;