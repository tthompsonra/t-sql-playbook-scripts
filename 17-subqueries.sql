USE Chinook;

GO

-- SUBQUERY
-- Using a column as an expression
-- This sub query select count(1) from
-- the album table where Album.ArtistId matches the Arist.ArtistId
-- And we alias this columnn with AlbumCount
-- Then we order it by AlbumCount in ASC order (lowest to highest scrolling down)
SELECT *,
(SELECT COUNT(1) FROM 
	Album WHERE Album.ArtistId = Artist.ArtistId
) as AlbumCount
FROM Artist
ORDER BY AlbumCount ASC;
