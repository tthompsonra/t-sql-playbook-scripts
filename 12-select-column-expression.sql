USE Chinook;

GO

-- Column Expressions 
-- Here we are concatenating the FirstName and LastName fields and alias
-- it to Customer Name
SELECT FirstName + ' ' + LastName AS 'Customer Name',
Email, Country
FROM Customer;