USE Chinook;
GO

-- Using a LEFT JOIN here versus a Subquery
SELECT worker.FirstName, worker.LastName, bosses.FirstName + ' ' + bosses.LastName AS Boss
FROM Employee worker
LEFT JOIN Employee bosses
ON worker.ReportsTo = bosses.EmployeeId;