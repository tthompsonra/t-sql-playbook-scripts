USE Chinook;

GO

INSERT INTO Users(Email)
VALUES ('travis@test.com');

SELECT Email, Name
FROM Users
INNER JOIN UserRole ON Users.Id = UserRole.UserId
INNER JOIN Role ON Role.Id = UserRole.RoleId;

SELECT Email, Name
FROM Users
LEFT JOIN UserRole ON Users.Id = UserRole.UserId
LEFT JOIN Role ON Role.Id = UserRole.RoleId

SELECT * FROM UserRole;


CREATE TABLE Users(
	Id INTEGER PRIMARY KEY NOT NULL IDENTITY(1, 1),
	Email VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE UserRole(
	UserId INTEGER,
	RoleId INTEGER,
	PRIMARY KEY(UserId, RoleId)
);

CREATE TABLE Role(
	Id INTEGER PRIMARY KEY NOT NULL IDENTITY(1, 1),
	Name VARCHAR(50) NOT NULL UNIQUE
);
