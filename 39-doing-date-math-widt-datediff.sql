USE Chinook;

GO

SELECT InvoiceId, InvoiceDate, Total, 
DATEPART(quarter, InvoiceDate) as quarter,
DATEPART(month, InvoiceDate) as month,
DATEPART(year, InvoiceDate) as year,
DATEPART(day, InvoiceDate) as day,
DATEDIFF(month, '01/01/2005', InvoiceDate) as MonthsInBusiness
FROM Invoice
ORDER BY MonthsInBusiness;